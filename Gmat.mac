/*                                                                   */
/*-------------------------------------------------------------------*/
/*                   G-matrix formal calculation                     */
/*-------------------------------------------------------------------*/
/*                                                                   */
/* V. Boudon (ICB) - April 2022 - Under license GPLv3                */
/*                                                                   */
/* vG:  table of G values (real or imaginary part)                   */
/* pG:  " " if vG real, "i" if vG imaginary                          */
/* pGi: 1 if vG real, %i if vG imaginary                             */
/* vG,pG,pGi[j+1,n+1,iC,iS,m+j+1]                                    */
/* Storage of vG and pGi coefficients in file "G"                    */
/*                                                                   */
/* WARNING: when multiplicity > 2, calculation is numerical          */
/*          (Maxima cannot diagonalize correctly the corresponding   */
/*           matrices in an exact form)                              */
/*                                                                   */
/* Method and numerical values in Refs:                              */
/* [1] J. Moret-Bailly et al., J. Mol. Spectrosc. 15, 355-377 (1965) */
/* [2] J. P. Champion et al., Can. J. Phys. 55, 512–520 (1977)       */
/* [3] M. Rey et al., J. Mol. Spectrosc. 219, 313-325 (2003)         */
/*                                                                   */

/*                                          */
/* Initializations and libraties to load    */
/*                                          */
kill(all)$
load("clebsch_gordan")$
load("eigen")$
load("/Users/vboudon/Documents/Maxima/G/djmmp")$      /* Companion program for Wigner matrices */
batch("/Users/vboudon/Documents/Maxima/G/nbjc")$      /* Companion program for irrep multiplicities for a given J value */
batch("/Users/vboudon/Documents/Maxima/G/sorting")$   /* Companion program to give sorting index */

hermitianmatrix:true$                                 /* Mandatory flag:                                                                   */
                                                      /* forces Gram-Schmidt orthonormalization for eigenvectors of degenerate eigenvalues */

/*                                          */
/* Maximum J value                          */
/*                                          */
jmax:12$

/*                                          */
/* Useful tables                            */
/*                                          */
G4:[sqrt(5/24),sqrt(7/12),sqrt(5/24)]$                /* G(4,4A1) coefficients */
plambda:[[0,0,0,2,2],[1,-1/2,0,-1,0],[1,3,4,2,5]]$    /* p value (m,m'=4*l+p), Wigner d matrix eigenvalue and corresponding irrep number */
irrep:["A1","A2","E ","F1","F2"]$                     /* irreps */
idim:[1,1,2,3,3]$                                     /* Dimension of irreps */
irrepc:["1","1","1","z","z"]$                         /* Name of first  calculated component of each irrep */
irrepcn:[1,1,1,3,3]$                                  /* Code of first  calculated component of each irrep */
irrepc2:[" "," ","2","x","x"]$                        /* Name of second calculated component of each irrep */
irrepc2n:[0,0,2,1,1]$                                 /* Code of second calculated component of each irrep */
irrepc3:[" "," "," ","y","y"]$                        /* Name of third  calculated component of each irrep */
irrepc3n:[0,0,0,2,2]$                                 /* Code of third  calculated component of each irrep */
phase:[[1,1,1,%i,%i],[%i,%i,%i,1,1]]$                 /* m max phase for J even and odd */

/*                                                        */
/* Output text files                                      */
/* to check numerical values                              */
/*                                                        */
/* NB: We use below the printf function                   */
/*     This one follows the LISP synthax for formatting   */
/*     See:                                               */
/*     https://en.wikipedia.org/wiki/Format_(Common_Lisp) */
/*                                                        */
fgout:openw("/Users/vboudon/Documents/Maxima/G/G.txt");
                                                       /* File with G coefficients (float values) in calculation order */
fgoutorder:openw("/Users/vboudon/Documents/Maxima/G/Gorder.txt");
                                                       /* File with G coefficients (float vaules) in stardard order */

/*                                                        */
/* Output LaTeX file                                      */
/*                                                        */
fgoutlatex:openw("/Users/vboudon/Documents/Maxima/G/Glatex.tex");

/*                                   */
/* Reference G values for comparison */
/*                                   */
Gref:read_matrix(file_search("/Users/vboudon/Documents/Maxima/G/Gref.txt"))$

/*                            */
/* J = 0 and J = 1 components */
/*                            */
print("-----------")$
print("J = ",0)$
print("-----------")$
outp:printf(fgout,"~%")$
G0:1$                                                    /* 0,A1 component */
vG[1,1,1,1,1]:G0$
pG[1,1,1,1,1]:"1"$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",0,0,0,irrep[1],irrepc[1],float(G0), " ")$
print("-----------")$
print("J = ",1)$
print("-----------")$
outp:printf(fgout,"~%")$
G1z:zeromatrix(3,1)$
G1z[2,1]:1$                                              /* 1,F1z component */
vG[2,1,4,3,2]:G1z[2,1]$
pG[2,1,4,3,2]:"1"$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",1,0,0,irrep[4],irrepc[4],float(G1z[2,1]), " ")$
kill(DD)$
DD:zeromatrix(3,3)$
for im:1 thru 3 do                                       /* Wigner matrix for C3(1,1,1) */
  (for imp:1 thru 3 do
     (DD[im,imp]:expand(djmmpC3(1,im-2,imp-2))
     )
  )$
print("DD =",DD)$
G1x:transpose(transpose(G1z).DD)$                        /* 1,F1x component */
vG[2,1,4,1,1]:G1x[1,1]$
pG[2,1,4,1,1]:"1"$
vG[2,1,4,1,3]:G1x[3,1]$
pG[2,1,4,1,3]:"1"$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",1,0,-1,irrep[4],irrepc2[4],float(G1x[1,1]), " ")$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",1,0,1,irrep[4],irrepc2[4],float(G1x[3,1]), " ")$
G1y:transpose(transpose(G1z).(DD.DD))$                   /* 1,F1y component */
vG[2,1,4,2,1]:imagpart(G1y[1,1])$
pG[2,1,4,2,1]:"i"$
vG[2,1,4,2,3]:imagpart(G1y[3,1])$
pG[2,1,4,2,3]:"i"$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",1,0,-1,irrep[4],irrepc3[4],float(imagpart(G1y[1,1])), "i")$
outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",1,0,1,irrep[4],irrepc3[4],float(imagpart(G1y[3,1])), "i")$

/*                                          */
/* Calculation                              */
/*                                          */
for j:2 thru jmax do                                     /* J loop */
  (parity:mod(j,2)+1,                                    /* J parity */
   print("-----------"),
   print("J = ",j),
   print("-----------"),
   for p:0 thru 2 step 2 do                              /* p loop: m,m'=4*l+p, p=0 or 2 */
     (print("p = ",p),
      lmin:-fix((j+p)/4),                                /* Minimum l value */
      lmax:fix((j-p)/4),                                 /* Maximum l value */
      d:lmax-lmin+1,                                     /* Full matrix dimension for these J and p */
      kill(A),                                           /* Initialize some matrices */
      kill(D),
      kill(L),
      kill(M),
      A:zeromatrix(d,d),                                 /* A: the matrix to diagonalize */
      D:zeromatrix(d,d),                                 /* D: Wigner d matrix */
      L:zeromatrix(1,d),                                 /* L: vector of l values */
      M:zeromatrix(1,d),                                 /* M: vector of m values */
      for l:lmin thru lmax do                            /* Loop on l */
        (m:4*l+p,                                        /* m value */
         ix:l-lmin+1,                                    /* index of l value for A, D, L and M */
         L[1,ix]:l,
         M[1,ix]:m,
         for lp:lmin thru lmax do                        /* Loop on l' */
           (mp:4*lp+p,                                   /* m' value */
            iy:lp-lmin+1,                                /* index of l' value for A and D */
            A[ix,iy]:radcan(A[ix,iy]+sum(G4[q4+2]*(-1)^m*wigner_3j(4,j,j,4*q4,-m,mp),q4,-1,1)),
                                                         /* A matrix calculation */
                                                         /* WARNING: [3] has (-1)^(j-m) instead of (-1)^m un [2] and this changes eigenvalue order and thus n */
                                                         /* WARNING: Although [3] looks correct, we kept A definiton from [2] to get the same n values        */
            D[ix,iy]:radcan(djmmp(j,m,mp,%pi/2))         /* D (Wigner d) matrix caclulation */
           )
        ),
      print("L = ",L),                                   /* Show matrices and vectors */
      print("M = ",M),
      print("A = ",A),
      print("D = ",D),
      E:eivects(D),                                      /* Diagonalization of D (Wigner d) matrix */
      Ev:E[1][1],                                        /* Eigenvalues */
      Ed:E[1][2],                                        /* Degeneracies */
      lE:length(Ev),                                     /* Number of eigenvalues */
      print("Wigner eigenvalues: ",Ev),
      for iE:1 thru lE do                                /* Loop on Wigner d eigenvalues */
        (for comp:1 thru 5 do                            /* Loop on the five "initial components to search for: A1, E1, F1z, E2, F2z */
           (if(p=plambda[1][comp]) then                  /* Does p correspond to this component ? */
              (if(Ev[iE]=plambda[2][comp]) then          /* Does the Wigner d eigenvalue correspond to the expected one for this compoment? */
                 (deg:Ed[iE],                            /* Degenaracy in this case */
                  print(irrep[plambda[3][comp]],irrepc[plambda[3][comp]],"degeneracy: ",deg),
                  Vt:conjugate(funmake('matrix,E[2][iE])),
                                                         /* Store eigenvectors in a matrix; here we conjugate since we work on the "bras"  */
                  for ideg:1 thru deg do                 /* Normalize these eigenvectors */
                    (NVt:sqrt(Vt[ideg].Vt[ideg]),
                     Vt[ideg]:Vt[ideg]/NVt
                    ),
                  V:conjugate(transpose(Vt)),            /* Adjoint: "kets" */
                  kill(Ar),                              /* Initialize restricted A matrix */
                  Ar:zeromatrix(deg,deg),
                  Ar:Ar+radcan(Vt.A.V),                  /* Restricted A matrix for the desired "initial" component we search for */
                  print("Wigner eigenvectors: ",V),
                  print("Restricted A =",Ar),
                  numflag:false,                         /* Flag to tell is the solution will be exact or numerical */
                  if(deg < 3) then
                    (Er:eivects(Ar)                      /* Diagonalize restricted A matrix exactly when dimension < 3 */
                    ) else
                    (Ernum:eigens_by_jacobi(Ar),         /* Diagonalize restricted A matrix numerically when dimension > 2 */
                     Vrnum:Ernum[2],
                     Ernums:sort(Ernum[1]),              /* Sorting everything in increasing eigenvelue order */
                     lrank:ranks(Ernum[1]),
                     Vrnumst:zeromatrix(deg,deg),
                     for inum:1 thru deg do
                       (for jnum:1 thru deg do
                          (Vrnumst[inum,lrank[jnum]]:Vrnum[inum,jnum]
                          )
                       ),
                     Vrnums:args(transpose(Vrnumst)),
                     Er:[[Ernums,makelist(1,i,length(Ernums))],makelist([Vrnums[i]],i,length(Ernums))],
                                                         /* Arrange results of eigens_by_jacobi in the same form as for eivects */
                     numflag:true                        /* Numerical calculation in this case */
                    ),
                  kill(Vrt),                             /* Restricted A matrix eigenvectors to be placed in a matrix over the unrestricted basis set */
                  Vrt:zeromatrix(deg,deg),
                  for ideg:1 thru deg do                 /* Again, we conjugate since we work on the "bras"  */
                    (if(numflag) then
                       (Vri:conjugate(Er[2][ideg][1])    /* Numerical calculation case */
                       ) else
                       (Vri:conjugate(radcan(Er[2][ideg][1]))
                                                         /* Exact calculation case */
                       ),
                    Vrt[ideg]:Vri
                    ),
                  Vr:conjugate(transpose(Vrt)),          /* Adjoint: "kets" */
                  print("Restricted A eigenvalues (float):",float(Er[1][1])),
                  print("Restricted A eigenvectors:",Vr),
                  kill(Gr),
                  Gr:zeromatrix(d,deg),
                  Gr:Gr+V.Vr,                            /* Restricted A matrix eigenvectors in the initial basis set */
                  if(numflag) then
                    (Gr:float(Gr)
                    ),
                  print(irrep[plambda[3][comp]],irrepc[plambda[3][comp]],"eigenvectors:",Gr),
                  Grt:conjugate(transpose(Gr)),          /* Adjoint */
                  kill(Grnt),
                  Grnt:zeromatrix(deg,d),
                  for ideg:1 thru deg do
                    (NGrti:sqrt(Grt[ideg].Grt[ideg]),    /* Normalize A matrix eigenvectors */
                     Grnt[ideg]:Grt[ideg]/NGrti,
                     ph:Grnt[ideg][d]/abs(Grnt[ideg][d]), 
                     Grnt[ideg]:conjugate(Grnt[ideg]/ph*phase[parity][plambda[3][comp]])
                                                         /* Set the phase for max m value; here we conjugate since we work on the "bra" */
                    ),
                  if(numflag) then
                     (Grnt:float(Grnt)
                     ),
                  Grn:conjugate(transpose(Grnt)),        /* Adjoint */
                  print(irrep[plambda[3][comp]],irrepc[plambda[3][comp]],"normalized eigenvectors:",Grn),
                  print(irrep[plambda[3][comp]],irrepc[plambda[3][comp]],"normalized eigenvectors (float):",float(Grn)),
                  print("Diagonalized restricted A matrix (float):",float(Grnt.A.Grn)),
                  outp:printf(fgout,"~%"),
                  for ideg:1 thru deg do                 /* Loop to store the resulting G matrix and write it in floating point format in the output file */
                    (for icomp:1 thru d do
                       (Gp:realpart(Grn[icomp][ideg]),   /* If G is real */
                        Gim:"1",
                        if(Gp=0) then
                          (Gp:imagpart(Grn[icomp][ideg]),/* If G is imaginary */
                           if(Gp#0) then                 /* Avoid 0i */
                             (Gim:"i"
                             )
                          ),
                        if(not(numflag)) then
                          (Gpraw:Gp,
                           Gp:rootscontract(radcan(ratsimp(radcan(Gpraw)))),
                                                         /* Simplification, but the radcan() function can introduce a sign error due to square roots of negative numbers */
                           Gpsig:rationalize(signum(float(Gp)*conjugate(float(Gpraw)))),
                                                         /* Find the correct sign using the ratio of float values for raw and simplified expressions */
                           Gpcorr:ratsimp(Gp*Gpsig)      /* Apply sign correction */
                          ) else
                          (Gpcorr:Gp
                          ),
                        vG[j+1,ideg,plambda[3][comp],irrepcn[plambda[3][comp]],M[1,icomp]+j+1]:Gpcorr,
                                                         /* G matrix storage */
                        pG[j+1,ideg,plambda[3][comp],irrepcn[plambda[3][comp]],M[1,icomp]+j+1]:Gim,
                                                         /* Imaginary flag storage */
                        Gimp:Gim,
                        if(Gim="1") then
                          (Gimp:" "
                          ),
                        outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",j,M[1,icomp],ideg-1,irrep[plambda[3][comp]],irrepc[plambda[3][comp]],float(Gpcorr),Gimp)
                       )
                    ),
                  if((comp # 1) and (comp # 4)) then
                    (kill(DD),
                     kill(MM),
                     dj:2*j+1,
                     DD:zeromatrix(dj,dj),
                     GM:zeromatrix(deg,dj),
                     for ideg:1 thru deg do
                       (for icomp:1 thru d do
                          (GM[ideg,M[1,icomp]+j+1]:Grn[icomp][ideg]
                                                         /* Create a vector for the "initial" component in the full [J,m> basis set */
                          )
                       ),
                     for im:1 thru dj do                 /* Wigner matrix for C3(1,1,1) */
                       (for imp:1 thru dj do
                          (DD[im,imp]:expand(djmmpC3(j,im-j-1,imp-j-1))
                          )
                       ),
                     print("DD =",DD),
                     print("GM =",GM),
                     cGM2:1,
                     aGM2:0,
                     if(comp=2) then                     /* Coefficients for E2 component */
                                                         /* WARNING: In this case formula 31 of [3] is false; the correct one is formula 16 of [2] */
                       (cGM2:2/sqrt(3),
                        aGM2:GM/2
                       ),
                     GM2raw:cGM2*(aGM2+GM.DD),           /* Compute E2, F1x or F2x component */
                     if(numflag) then                    /* Numerical calculation case */
                       (GM2corr:float(GM2raw),
                        print("Second component:",GM2corr)
                       ) else                            /* Exact calculation case */
                       (GM2:rootscontract(radcan(ratsimp(radcan(GM2raw)))),
                                                         /* Simplification, but the radcan() function can introduce a sign error due to square roots of negative numbers */
                        GM2sig:rationalize(signum(float(GM2*conjugate(float(GM2raw))))),
                                                         /* Find the correct sign using the ratio of float values for raw and simplified expressions */
                        print("Second component:",GM2),
                        print("Second component (float):",float(GM2)),
                        print("Second component (phase):",GM2sig),
                        GM2corr:ratsimp(GM2*GM2sig),     /* Apply sign correction */
                        print("Second component corrected (float):",float(GM2corr))
                       ),
                     for ideg:1 thru deg do              /* Loop to store the resulting G matrix and write it in floating point format in the output file */
                       (for icompj:1 thru dj do
                          (G:GM2corr[ideg,icompj],
                           if(abs(float(G)) > 1E-10) then
                             (Gp:realpart(G),            /* If G is real */
                              Gim:"1",
                              if(Gp=0) then
                                (Gp:imagpart(G),         /* If G is imaginary */
                                 if(Gp#0) then           /* Avoid 0i */
                                    (Gim:"i"
                                    )
                                ),
                               vG[j+1,ideg,plambda[3][comp],irrepc2n[plambda[3][comp]],icompj]:Gp,
                                                         /* G matrix storage */
                               pG[j+1,ideg,plambda[3][comp],irrepc2n[plambda[3][comp]],icompj]:Gim,
                                                         /* Imaginary flag storage */
                               Gimp:Gim,
                                 if(Gim="1") then
                                   (Gimp:" "
                                   ),
                               outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",j,icompj-j-1,ideg-1,irrep[plambda[3][comp]],irrepc2[plambda[3][comp]],float(Gp),Gimp)
                             )
                          )
                       ),
                     if(comp # 2) then
                       (GM3raw:GM.(DD.DD),               /* Compute F1y or F2y component using C3(1,1,1)^2 */
                        if(numflag) then                 /* Numerical calculation case */
                          (GM3corr:float(GM3raw),
                           print("Third component:",GM3corr)
                          ) else                         /* Exact calculation case */
                          (GM3:rootscontract(radcan(ratsimp(radcan(GM3raw)))),
                                                         /* Simplification, but the radcan() function can introduce a sign error due to square roots of negative numbers */
                           GM3sig:rationalize(signum(float(GM3*conjugate(float(GM3raw))))),
                                                         /* Find the correct sign using the ratio of float values for raw and simplified expressions */
                           print("Third component:",GM3),
                           print("Third component (float):",float(GM3)),
                           print("Second component (phase):",GM3sig),
                           GM3corr:ratsimp(GM3*GM3sig),  /* Apply sign correction */
                           print("Second component corrected (float):",float(GM3corr))
                          ),
                        for ideg:1 thru deg do           /* Loop to store the resulting G matrix and write it in floating point format in the output file */
                          (for icompj:1 thru dj do
                             (G:GM3corr[ideg,icompj],
                              if(abs(float(G)) > 1E-10) then
                                (Gp:realpart(G),         /* If G is real */
                                 Gim:"1",
                                 if(Gp=0) then
                                   (Gp:imagpart(G),      /* If G is imaginary */
                                    if(Gp#0) then        /* Avoid 0i */
                                      (Gim:"i"
                                      )
                                   ),
                                 vG[j+1,ideg,plambda[3][comp],irrepc3n[plambda[3][comp]],icompj]:Gp,
                                                         /* G matrix storage */
                                 pG[j+1,ideg,plambda[3][comp],irrepc3n[plambda[3][comp]],icompj]:Gim,
                                                         /* Imaginary flag storage */
                                 Gimp:Gim,
                                 if(Gim="1") then
                                   (Gimp:" "
                                   ),
                                 outp:printf(fgout,"~4d~5d~4d ~a ~a ~18,15f ~a~%",j,icompj-j-1,ideg-1,irrep[plambda[3][comp]],irrepc3[plambda[3][comp]],float(Gp),Gimp)
                                )
                             )
                          )
                       )
                    )
                 )
              )
           )
        )
     )
  )$

/*                                                                    */
/* Output to ordered text file                                        */
/* Numerical values ordered by j,n,C,s,m                              */
/* Coffecients in the same format and order as from the GROUP program */
/*                                                                    */
irreps:[["1","1","1","x","x"],[" "," ","2","y","y"],[" "," "," ","z","z"]]$
                                                         /* Irrep components */
outp:printf(fgoutorder,"~%")$
outp:printf(fgoutorder,"G matrix elements (Jmin=  0, Jmax=~3d)~%",jmax)$
outp:printf(fgoutorder,"~%")$
outp:printf(fgoutorder,"   J    M   N  C s  G                      Ratio   Exact value~%")$
                                                         /* File header */
igref:0$
for j:0 thru jmax do
  (mult:nbjc(j),
   for iC:1 thru 5 do
     (if(mult[iC]>0) then
        (outp:printf(fgoutorder,"~%"),                   /* Blank line before each J,C */
         outt:printf(fgoutlatex,"\\section*{\\boldmath$J=~d, C=~a_{~a}$}~%",j,charat(irrep[iC],1),charat(irrep[iC],2)),
                                                         /* A section for each J,C in the LaTeX output */
         for in:1 thru mult[iC] do
           (for is:1 thru idim[iC] do
              (for m:-j thru j do
                 (gex:vG[j+1,in,iC,is,m+j+1],            /* Exact value */
                  gval:float(gex),                       /* Numerical value */
                  if(numberp(gval)) then                 /* Test if value exists */
                    (igref:igref+1,
                     gtest:Gref[igref][1],
                     if(gtest=0.0) then                  /* Case of acidentally zero value */
                       (gtest:1
                       ),
                     grat:float(gval/gtest),             /* Ratio with reference numerical values */
                     if(grat=0.0) then                   /* Case of acidentally zero value */
                       (grat:1.0                         /* We put ratio to 1 although it is 0/0 to mean both values are identical */
                       ),
                     Gimp:pG[j+1,in,iC,is,m+j+1],
                     pGi[j+1,in,iC,is,m+j+1]:parse_string(subst("i"="%i",Gimp)),
                                                         /* Table for formal phase part: 1 or %i */
                     if(Gimp="1") then
                       (Gimp:" "
                       ),
                     gext:tex1(gex),
                     gext:ssubst("\\;","\\,",gext),      /* Some LaTeX output formatting */
                     gext:ssubst("\\;","\\cdot",gext),
                     outt:printf(fgoutlatex,"$${}^{(~d)}G_{~d~a_{~a}~a}^{~d}=~a\\;~a$$~%",j,in-1,charat(irrep[iC],1),charat(irrep[iC],2),irreps[is][iC],m,gext,Gimp),
                                                         /* LaTeX output */
                     if(not(floatnump(gex))) then
                       (outp:printf(fgoutorder,"~4d~5d~4d ~a ~a ~18,15f ~a   ~6,3f   ~a~%",j,m,in-1,irrep[iC],irreps[is][iC],gval,Gimp,grat,gex)
                                                         /* Formal output */
                       ) else
                       (outp:printf(fgoutorder,"~4d~5d~4d ~a ~a ~18,15f ~a   ~6,3f   ~18,15f~%",j,m,in-1,irrep[iC],irreps[is][iC],gval,Gimp,grat,gex)
                                                         /* Numerical output */
                       )
                    )
                 )
              )
           ) 
        )
     )
  )$
/*                                          */
/* Save G matrix and phase for further use  */
/*                                          */
save("/Users/vboudon/Documents/Maxima/G/G",vG,pGi)$

/*                                          */
/* End of program                           */
/*                                          */
close(fgout)$
close(fgoutorder)$
close(fgoutlatex)$

