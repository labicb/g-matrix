G-matrix formal calculation program using Maxima.                     
V. Boudon (ICB) - April 2022 - Under license GPLv3  

Calculation of G orientation matrix coefficients to symmetrize spherical
wave functions or tensor operators in the Td point group.

Method and numerical values to be found in Refs:                              
- J. Moret-Bailly et al., J. Mol. Spectrosc. 15, 355-377 (1965) 
- J. P. Champion et al., Can. J. Phys. 55, 512–520 (1977)       
- M. Rey et al., J. Mol. Spectrosc. 219, 313-325 (2003)         

This code (Gmat.mac) is to be run using the Maxima software:
https://maxima.sourceforge.io

It is mandatory to change all file paths in Gmat.mac
(batch, load and save commands).        

Gmat has to be launched thanks to the "batch()" command:
batch("/path_to_gmat_directory/Gmat");

It uses 3 companion Maxima programs: djmmp.mac, nbjc.mac, sorting.mac
and a companion text file for testing purpose: Gref.txt.

WARNING: when multiplicity > 2, calculation is numerical.     
Maxima cannot diagonalize the corresponding matrices in an exact form.
                                        
Gmat has been tested for J up to jmax = 12.

Output files:

- G.txt: the G coefficients (float values) in calculation order.
- Gorder.txt: the G coefficients (float values) in standard order.
- Glatex.tex: LaTeX output file of G coefficient expressions.
- G: the G coefficients for further reuse.
